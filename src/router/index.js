import Vue from 'vue'
import VueRouter from 'vue-router'
import layout from '@/views/layout'
import home from '@/views/home'
import shibie from "@/views/shibie/index";
import shibieFile from '@/views/shibie-file'
import appleDesease from '@/views/apple'
import shibieFiles from '@/views/shibie-files'
import paizhao from '@/views/paizhao'
import shishi from '@/views/shishi/index2'
import baike from '@/views/baike'
import Medication from '@/views/Medication'
import xiangxi from '@/views/xiangxi/index'
import xiangxi2 from '@/views/xiangxi/index2'
import xiangxi3 from '@/views/xiangxi/index3'
import rightDetail from '@/views/rightDetail'
import search from '@/views/search'
import search2 from '@/views/search/index2'
import search3 from '@/views/search/index3'
import index1 from '@/views/center/index1'
import index2 from '@/views/center/index2'
import index3 from '@/views/center/index3'
import new1 from '@/views/center/new1'
import new2 from '@/views/center/new2'
import new3 from '@/views/center/new3'
import new4 from '@/views/center/new4'
import new5 from '@/views/center/new5'
import new6 from '@/views/center/new6'
import new7 from '@/views/center/new7'
import new8 from '@/views/center/new8'
import new9 from '@/views/center/new9'
import new10 from '@/views/center/new10'
import new11 from '@/views/center/new11'
import new12 from '@/views/center/new12'
import new13 from '@/views/center/new13'
import new14 from '@/views/center/new14'
import new15 from '@/views/center/new15'
import new16 from '@/views/center/new16'
import new17 from '@/views/center/new17'
import new18 from '@/views/center/new18'
import new19 from '@/views/center/new19'
import new20 from '@/views/center/new20'
// import { component } from 'vue/types/umd'
// import { component } from 'vue/types/umd'

// import { component } from 'vue/types/umd'
const originalPush = VueRouter.prototype.push
   VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}
Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    component:layout,
    redirect:'/home',
    children:[
      {
        path:'home',
        component:home
      },
      {
        path:'shibie',
        component:shibie,
        children:[
          {
            path:'shibieFile',
            component:shibieFile,
            children:[
              {
                path:'appleDesease',
                component:appleDesease
              }
            ]
          },
          {
            path:'shibieFiles',
            component:shibieFiles
          },
          {
            path:'paizhao',
            component:paizhao,
            children:[
              {
                path:'appleDesease',
                component:appleDesease
              }
            ]
          },
          {
            path:'shishi',
            component:shishi
          }
        ]
      },
      {
        path:'baike',
        component:baike,
        children:[
          {
            path:'rightDetail',
            component:rightDetail,
          },
          
        ]
      },
      {
        path:'Medication',
        component:Medication,
        children:[
          {
            path:'index1',
            component:index1
          },
          {
            path:'index2',
            component:index2,         
          },
          {
            path:'index3',
            component:index3,         
          }
        ]
      },
      {
        path:'search',
        component:search,
        children:[
          {
            path:'search2',
            component:search2
          },
          {
            path:'search3',
            component:search3
          },
        ]
      },
    ]
  },
  {
    path:'/new1',
    component:new1
  },
  {
    path:'/new2',
    component:new2
  }
  ,
  {
    path:'/new3',
    component:new3
  },
  {
    path:'/new4',
    component:new4
  },
  {
    path:'/new5',
    component:new5
  },
  {
    path:'/new6',
    component:new6
  },
  {
    path:'/new7',
    component:new7
  },
  {
    path:'/new8',
    component:new8
  },
  {
    path:'/new9',
    component:new9
  },
  {
    path:'/new10',
    component:new10
  },
  {
    path:'/new11',
    component:new11
  },
  {
    path:'/new12',
    component:new12
  },
  {
    path:'/new13',
    component:new13
  },
  {
    path:'/new14',
    component:new14
  },
  {
    path:'/new15',
    component:new15
  },
  {
    path:'/new16',
    component:new16
  },
  {
    path:'/new17',
    component:new17
  },
  {
    path:'/new18',
    component:new18
  },
  {
    path:'/new19',
    component:new19
  },
  {
    path:'/new20',
    component:new20
  },
  {
    path:'/xiangxi',
    component:xiangxi
  },
  {
    path:'/xiangxi2',
    component:xiangxi2
  },
  {
    path:'/xiangxi3',
    component:xiangxi3
  },
]

const router = new VueRouter({
  routes
})
// import VueRouter from 'vue-router'

Vue.use(VueRouter)
export default router
