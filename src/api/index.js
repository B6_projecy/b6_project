import request from "../utils/request";
//单个图片
export const predict_single = (data) => {
  return request({
    method: "POST",
    url: "/predict/predict_single",
    data,
  });
};
//批量
export const predict_batch = (data) => {
  return request({
    method: "POST",
    url: "/predict/predict_batch",
    data,
  });
};
//拍照及实时
export const photoOrVdieo = (data) => {
  return request({
    method: "POST",
    url: "/predict/take_photo",
    data,
    headers: {
      "Content-Type": "json/application",
    },
  });
};
//根据病害名称查询病害详情
export const plantDetil = (str) => {
  return request({
    method: "GET",
    url: "/plant/PlantDetil/" + str,
  });
};

//根据病害id查询病害详情
export const plantDetilById = (str) => {
  return request({
    method: "GET",
    url: "/plant/Plant/" + str,
  });
};

// 百科菜单栏获取
export const menuBar = () => {
  return request({
    method: "GET",
    url: "/plant/menuBar/",
  });
};
//获取某植物的所有病害，并分页
export const plants = (plantName = "番茄", currentPage = 1, pageSize = 16) => {
  return request({
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    },
    method: "GET",
    params: {
      plantName,
      currentPage,
      pageSize,
    },
    url: "/plant/plantName/",
  });
};

// + plantName + "/" + currentPage + "/" + pageSize

// 搜索
export const search = (str, currentPage = 1, pageSize = 16) => {
  return request({
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    },
    method: "GET",
    url: "/plant/search/",
    params: {
      str,
      currentPage,
      pageSize,
    },
  });
};

// 下载
export const download = (path) => {
  return request({
    url: "/predict/download",
    method: "GET",
    responseType: "blob",
    params: {
      path,
    },
  });
};
