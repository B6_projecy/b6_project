import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    avatar: "", //存储单个图片，用于单个识别，图片预览
    menuBar: false, //是否在识别页（顶部菜单栏的识别），true代表是
    avatars: [],
    imgs: {}, //选择的图片
    vdieoStatus: false, // 摄像头状态,也是是否打开识别
    disease: "", // 存储病害名称，识别页展示病害详情
    str: "", // 要搜索的字符串
  },
  getters: {},
  //相当于javabean的setter方法，用于改变state中定义的变量值
  mutations: {
    avatar(state, val) {
      state.avatar = val;
      var Number = localStorage.getItem("number");
      // console.log(number)
      localStorage.setItem("key" + Number, val);
      Number++;
      localStorage.setItem("number", Number);
    },
    avatars(state, val) {
      // tate.avatars = val;

      var Number = localStorage.getItem("number");
      var numberA = Number;
      for (var i = 0; i < state.avatars.length; i++) {
        if (val[i] != "undefined") {
          console.log(localStorage);
          localStorage.setItem("key" + numberA, val[i]);
          numberA++;
        }
      }
      localStorage.setItem("number", numberA);
      state.avatars = val;
    },
    setMenuBar(state, val) {
      state.menuBar = val;
    },
    setData(state, val) {
      state.imgs = val;
    },
    changeStatus(state, status) {
      state.vdieoStatus = status;
    },
    setDisease(state, val) {
      state.disease = val;
    },
    searchStr(state, val) {
      state.str = val;
    },
  },
  actions: {},
  modules: {},
});
